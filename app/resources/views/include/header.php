<!DOCTYPE html>
<?php

use Olson\ViewHelper;

?>
<html lang="en">
    <head>
        <title><?php echo htmlspecialchars($data['pageTitle']) ?></title>
        <link rel="stylesheet" type="text/css" href="<?php echo ViewHelper::getResourcesPath() . '/css/style.css' ?>" />
    </head>
    <body>
        <div id="container">

