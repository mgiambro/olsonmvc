<?php

namespace Example\Model\Impl;

use Example\Model\ExampleModelInterface;
use Example\Model\BaseModel;

/**
 * Description of ExampleModelImpl
 *
 * @author maurizio
 */
class ExampleModelImpl extends BaseModel implements ExampleModelInterface {

    public function isLeapYear($year = null)
    {
        if (null === $year) {
            $year = date('Y');
        }

        return 0 == $year % 400 || (0 == $year % 4 && 0 != $year % 100);
    }

}
