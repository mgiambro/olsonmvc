<?php

namespace Example\Model;

/**
 * Description of ArticleModelInterface
 *
 * @author maurizio
 */
interface ExampleModelInterface {
    
    public function isLeapYear($year = null);

}
