<?php

namespace Example\Controller;

use Example\Controller\BaseController;

class ExampleController extends BaseController {

    private $model;
    private $config;

    function __construct()
    {
        parent::__construct();
        $this->model = $this->services->get('ExampleModel');
        $this->config = $this->services->get('config');
    }

    public function indexAction($year)
    {
        $data = array();

        if ($this->model->isLeapYear($year)) {
            $response = 'Yep, this is a leap year! ' . rand();
        } else {
            $response = 'Nope, this is not a leap year.';
        }

        $date = date_create_from_format('Y-m-d H:i:s', '2005-10-15 10:00:00');

        $data['response'] = $response;
        $data['pageTitle'] = 'Olson MVC Example Page';
        $data['date'] = $date;

        return $this->templating->render('example.php', array('data' => $data));
    }

}
