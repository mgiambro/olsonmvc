<?php

// example.com/src/app.php
use Symfony\Component\Routing;

$routes = new Routing\RouteCollection();

// Article
$routes->add('is_leap_year', new Routing\Route('/is_leap_year/{year}', array(
    'year' => 2011,
    '_controller' => 'Example\\Controller\\ExampleController::indexAction',
)));

/*
$routes->add('test', new Routing\Route('/test/', array(
    '_controller' => 'Test\\Controller\\TestController::indexAction',
)));


$routes->add('leap_year', new Routing\Route('/is_leap_year/{year}', array(
    'year' => null,
    '_controller' => 'Calendar\\Controller\\LeapYearController::indexAction',
)));
*/


return $routes;


