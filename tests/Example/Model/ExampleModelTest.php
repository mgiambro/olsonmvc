<?php

namespace Test\Model;

use Example\Model\Impl\ExampleModelImpl;

/**
 * Description of ArticleModelTest
 *
 * @author maurizio
 */
class ArticleModelTest extends \PHPUnit_Framework_TestCase {

    public function testIsLeapYear()
    {
        $exampleModel = new ExampleModelImpl();
        
       $falseResult = $exampleModel->isLeapYear('2011');
  //      echo var_dump($result); exit;
       $this->assertFalse($falseResult);
       
       $trueResult = $exampleModel->isLeapYear('400');
       $this->assertTrue($trueResult);
       
    }

}
