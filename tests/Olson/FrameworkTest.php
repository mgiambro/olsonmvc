<?php

// example.com/tests/Olson/Tests/FrameworkTest.php

namespace Olson\Tests;

use Olson\Framework;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;

class FrameworkTest extends \PHPUnit_Framework_TestCase {

    public function testControllerResponse() {
        $matcher = $this->getMock('Symfony\Component\Routing\Matcher\UrlMatcherInterface');

        $matcher
                ->expects($this->once())
                ->method('match')
                ->will($this->returnValue(array(
                            '_route' => 'foo',
                            'name' => 'Maurizio',
                            '_controller' => function ($name) {
                                return new Response('Hello ' . $name);
                            }
                )))
        ;
        $matcher
                ->expects($this->once())
                ->method('getContext')
                ->will($this->returnValue($this->getMock('Symfony\Component\Routing\RequestContext')))
        ;

        $routerListener = new \Symfony\Component\HttpKernel\EventListener\RouterListener($matcher);

        $eventDispatcher = new \Symfony\Component\EventDispatcher\EventDispatcher;
        ;
        $eventDispatcher->addSubscriber($routerListener);

        $resolver = new ControllerResolver();

        $framework = new Framework($eventDispatcher, $resolver);

        $response = $framework->handle(new Request());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains('Hello Maurizio', $response->getContent());
    }

    public function testErrorHandling() {
        $framework = $this->getFrameworkForException(new \RuntimeException());

        $this->setExpectedException('RuntimeException');

        $response = $framework->handle(new Request());

        $this->assertEquals(500, $response->getStatusCode());
    }

    public function testNotFoundHandling() {
        $framework = $this->getFrameworkForException(new ResourceNotFoundException());
        $this->setExpectedException('Symfony\Component\HttpKernel\Exception\NotFoundHttpException');
        $response = $framework->handle(new Request());

        $this->assertEquals(404, $response->getStatusCode());
    }

    private function getFrameworkForException($exception) {
        $matcher = $this->getMock('Symfony\Component\Routing\Matcher\UrlMatcherInterface');
        $matcher
                ->expects($this->once())
                ->method('match')
                ->will($this->throwException($exception))
        ;
        $matcher
                ->expects($this->once())
                ->method('getContext')
                ->will($this->returnValue($this->getMock('Symfony\Component\Routing\RequestContext')))
        ;

        $routerListener = new \Symfony\Component\HttpKernel\EventListener\RouterListener($matcher);

        $eventDispatcher = new \Symfony\Component\EventDispatcher\EventDispatcher;
        ;
        $eventDispatcher->addSubscriber($routerListener);


        $resolver = new ControllerResolver();

        return new Framework($eventDispatcher, $resolver);
    }

}
