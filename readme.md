
**INSTALLATION:**

Olson MVC uses the Dependency Management tool Composer, so you'll need to have this installed to use it. If you don't currently have composer installed on your system, everything to get
up and running can be found at (https://getcomposer.org/"Composer").

To use Olson MVC, first create a new project directory and copy the following to that directory:


app  
src  
tests  
web  
.gitignore  
composer.json  
composer.lock  
index.php  
phpunit.xml.dist  
readme.md


Once this is set up, open up a command prompt, navigate to the project root and run the following command:

composer install

This will install all the third party dependencies.

**APPLICATION CONFIGURATION:**

Application settings are stored in an array in app/settings/settings.php. 

Given this settings array:


    $global_settings = array('project' => array('project_root' => 'http://localhost/projects/OlsonMVC/'), 'database' => array('dsn' => 'mysql:dbname=cms;host=127.0.0.1','host' => '127.0.0.1','port' => '3306','dbname' => 'cms','username' => 'root','password' => 'password',),
    )

To get the database dsn value:


    use Olson\Config\Configuration;

    $config = new Configuration();
    $dsn = $config->getSettings('database','dsn');


**ROUTING:**

Olson MVC uses the php version of the Symfony Routing component and more detail on this can be found on the (http://symfony.com/doc/current/components/routing/introduction.html"Symfony") website.

To configure a route, you simply add it to the src/app.php page in the following format:


    $routes->add('is_leap_year', new Routing\Route('/is_leap_year/{year}', array(
    'year' => 2011,
    '_controller' => 'Example\\Controller\\ExampleController::indexAction',
    )));


The above route uses the ExampleController which lives in Example/Controller and calls the indexAction method on that controller. The {year} after the /is_leap_year in the Route constructor
represents a variable that is passed into the controller.

To call this from your browser, your url would be something like:

    http://servername/projectname/web/front.php/is_leap_year/2016

Note the /web/front.php. This page is effectively the page which glues the components of the framework together.

**DEPENDENCY INJECTION CONTAINER:**

Key to the framework are the principles of clean, de-coupled code. This is why the framework uses the (http://symfony.com/doc/current/components/dependency_injection/introduction.html"Symfony DependencyInjection component")

A description of what dependency injection is and it's benefits are beyond the scope of this document, but there are plenty of references on the web if you'd like to read up.

Objects are configured, wired together in src/services.php.


    $sc->register('ExampleModel', 'Example\Model\Impl\ExampleModelImpl')->setArguments(array(new Reference('dbDao'), new Reference('config'), new Reference('logger')));
    $sc->register('dbDao', 'Olson\Da\Impl\DbPdoImpl')
    ->setArguments(array(new Reference('config'), new Reference('logger')));
    $sc->register('config', 'Olson\Config\Configuration');
    $sc->register('logger', 'Katzgrau\KLogger\Logger')->setArguments(array(__DIR__ .'/../app /data/logs')
    );


The above configuration shows four objects configured. 

The first argument of the register method is the name you give to your object. The second is the full path of the class.

In the above example, the config and logger objects are injected into the dbDao object and the dbDao is injected into the ExampleModel object via the setArguments method. 

Objects can be injected into classes either via the constructor or setters. The following example shows the ExampleModel and config objects shown above injected into a controller via constructor injection.


    namespace Example\Controller;

    use Example\Controller\BaseController;

    class ExampleController extends BaseController {

    private $model;
    private $config;

    function __construct()
    {
        parent::__construct();
        $this->model = $this->services->get('ExampleModel');
        $this->config = $this->services->get('config');
    }
    ...


In this code, the services object is inherited from BaseController. However you can organise your code as you wish.


    namespace Example\Controller;

    use Symfony\Component\Templating\PhpEngine;
    use Symfony\Component\Templating\TemplateNameParser;
    use Symfony\Component\Templating\Loader\FilesystemLoader;

    abstract class BaseController {

    protected $templating;
    protected $services;

    function __construct()
    {
        $loader = new FilesystemLoader(__DIR__ . '/../../../app/resources/views/%name%');
        $this->setServices();

        $this->templating = new PhpEngine(new TemplateNameParser(), $loader);
    //        $twig = new Twig_Environment($loader, array(
     //        'cache' => '/../../../app/resources/cache',
    //));
    }
    
    function setServices()
    {
        $this->services = include __DIR__ . '/../../../src/services.php';
    }
    }

**CONTROLLER**

Full explanation of the principles behind Model View Controller(MVC) is beyond the scope of this document. The single aim for this document is to explain how to implement MVC within this
framework.

A request will come in as set up in the previously exlpalined Routing section. This will typically direct to a controller. Let's take the following ArticleController as an example:

    class ArticleController extends BaseController {

    private $model;
    private $config;

    function __construct()
    {
        parent::__construct();
        $this->model = $this->services->get('ArticleModel');
        $this->config = $this->services->get('config');
        //      print "In SubClass constructor\n";
    }

    public function indexAction()
    {
        $homepageNumArticles = $this->config->getSettings('project', 'homepage_num_articles');
        $data = array();
        $articles = $this->model->getList($homepageNumArticles);
        $data['articles'] = $articles;
        $data['totalRows'] = sizeOf($articles);
        $data['pageTitle'] = 'Widget News';

        return $this->templating->render('homepage.php', array('data' => $data));
    }

In this example, model and config injected into the controller via the ArticleController constructor. The indexAction simply uses the config object to get a configuration setting, builds a list of articles using the injected model and renders them to homepage.php, passing in the articles in the $data array.

**MODEL**

Taking the following ArticleModel as an example:

    class ArticleModelImpl extends BaseModel implements ArticleModelInterface {

    private $config;
    private $dao;
    private $dbTable = 'articles';
    private $logger;

    public function __construct(DbDao $dbDao, Configuration $config, Logger $logger = null)
    {
        $this->dao = $dbDao;
        $this->logger = $logger;
        $this->config = $config;
    }

    public function getList($numRows = 1000000, $order = "publicationDate DESC")
    {
        $articles = array();
        $sql = "SELECT SQL_CALC_FOUND_ROWS *, UNIX_TIMESTAMP(publicationDate) AS publicationDate FROM articles
            ORDER BY " . $this->mysql_real_escape_string($order) . " LIMIT :numRows";

        $params = [['column' => ':numRows', 'value' => (int) trim($numRows), 'dataType' => PDO::PARAM_INT]];

        $qry = $this->dao->query($sql, $params);
        $rows = $qry->fetchAll();

        foreach ($rows as $row) {
            array_push($articles, Article::fromDbRow($row));
        }
        return $articles;
    }

The constructor injects a dbDao data access, logger and config classes.

The getList method passes a sql query to thequery method of the dao and returns a list of results.


**DATABASE ACCESS**

Olson MVC comes with a lightweight data access layer written over PDO. However, if you prefer to use another data layer such as doctrine, simply add it to composer.json.

The data layer which comes with Olson comprises the following methods:

PDO Statement __query__($sql, $parms = array());__

__Parameters__

sql     The sql query to be run.
params  An array of parameters that are passed in to form part of the query. The format of the array is as follws:

 $params = [['column' => ':numRows', 'value' => (int) trim($numRows), 'dataType' => PDO::PARAM_INT],
            ['column' => ':name', 'value' => 'Jonh Doe']];

Each param has three properties, the column name, the value and the data type. The datatype is optional and defaults to PDO::PARAM_STR.
   
PDO Statement __dbUpdate__($table, $ref, $fields, $fieldref = 'ref');

__Parameters__

table       The table to update
ref         The search value for the record to update
fields      An array of fields to update with their values. The format of the array is as follows:

 $params = [['column' => ':numRows', 'value' => (int) trim($numRows), 'dataType' => PDO::PARAM_INT],
            ['column' => ':name', 'value' => 'Jonh Doe']];

Each param has three properties, the column name, the value and the data type. The datatype is optional and defaults to PDO::PARAM_STR.

fieldref    The column name of the column to search for the ref to update.
    
int __dbInsert__($table, $fieldList);

__Parameters__

table       The table to update
fieldlist      An array of fields to update with their values. The format of the array is as follows:

 $params = [['column' => ':numRows', 'value' => (int) trim($numRows), 'dataType' => PDO::PARAM_INT],
            ['column' => ':name', 'value' => 'Jonh Doe']];


int __dbInsertUpdate__($table, $fields, $refField='ref', $ref = '');

__Parameters__

table       The table to update
ref         The search value for the record to update
fields      An array of fields to update with their values. The format of the array is as follows:

 $params = [['column' => ':numRows', 'value' => (int) trim($numRows), 'dataType' => PDO::PARAM_INT],
            ['column' => ':name', 'value' => 'Jonh Doe']];

PDO Statement __dbDelete__($table, $ref, $fieldref = 'ref');

__Parameters__

table       The table to update
ref         The search value for the record to update
fieldref    The column name of the column to search for the ref to update.

**VIEW TEMPLATING**

The default view templating of Olson MVC uses the Symfony PHPEngine templating. However, if you prefer twig or some other templating engine, simply add the component to composer.json.

Taking the earlier described indexAction in ArticleController which renders a list of articles in the view homepage.php:

        public function indexAction()
    {
        $homepageNumArticles = $this->config->getSettings('project', 'homepage_num_articles');
        $data = array();
        $articles = $this->model->getList($homepageNumArticles);
        $data['articles'] = $articles;
        $data['totalRows'] = sizeOf($articles);
        $data['pageTitle'] = 'Widget News';

        return $this->templating->render('homepage.php', array('data' => $data));
    }

The following shows how to render data returned by the controller in a view:

    <?php foreach ( $data['articles'] as $article ) { ?>
 
        <li>
          <h2>
            <span class="pubDate"><?php echo date('j F', $article->publicationDate)?></span><a href="../view-article/<?php echo $article->id?>"<?php echo htmlspecialchars( $article->title )?></a>
          </h2>
          <p class="summary"><?php echo htmlspecialchars( $article->summary )?></p>
        </li>
 
    <?php } ?>

As you can see from the above PHP code, the $data variable returned by the articleController is available to use in your PHP page.